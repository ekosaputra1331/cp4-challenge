// fixing display on small screen
$(window).on('resize', function(){
    if($(window).width() < 575){
        $('#user').removeClass('d-flex')
        $('#com').removeClass('d-flex')
    }else{
        $('#user').addClass('d-flex')
        $('#com').addClass('d-flex')
    }
})

// instanciate all needed object
const userPlayer = new UserPlayer();
const comPlayer = new ComPlayer();
const rps = new RockPaperScissor(userPlayer, comPlayer);

// rps.userTurn(RockPaperScissor.choices.SCISSOR);
// rps.comTurn()
const btnUser = document.querySelectorAll('#user .choice');
const btnCom = document.querySelectorAll('#com .choice');

// set init score
const score =  {
    userScore: document.getElementById('player-score'),
    comScore: document.getElementById('com-score'), 
    btnReset: document.getElementById('reset-btn')
}

score.userScore.innerText = "0";
score.comScore.innerText = "0";
score.btnReset.setAttribute('disabled', true);

// result
const result = {
    versus: document.getElementById('versus'),
    userWin: document.getElementById('userWin'),
    comWin: document.getElementById('comWin'),
    draw: document.getElementById('draw')
}

result.versus.style.display = 'block';

// user buttons
btnUser.forEach(function(el){
    el.addEventListener('click', function(){
        el.classList.add('active');

        // disable all buttons
        btnUser.forEach(function(el) {
            el.setAttribute('disabled', true);
        })
        
        // rps.
        rps.userTurn(el.dataset.choice);
        rps.comTurn(btnCom, score, result);
    })
})

// reset button
score.btnReset.addEventListener('click', function(el){
    btnUser.forEach(function(el){
        el.classList.remove('active');
        el.removeAttribute('disabled');
    })

    btnCom.forEach(function(el){
        el.classList.remove('active');
    })
    
    Object.values(result).forEach(function(el){
        if(el.id === 'versus'){
            return el.style.display = 'block';
        }
        return el.style.display = 'none';
    })
    
    this.setAttribute('disabled', true);
})